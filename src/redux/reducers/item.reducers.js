
const initialState = {
    posts: [],
    loading: false,
    error: ""
}

export default function PostReducer(state = initialState, action) {
    switch(action.type) {
        case 'NEW_POST_PENDING':
            return {
                ...state,
                loading: true
            }
        case 'NEW_POST_SUCCES':
                return {
                ...state,
                loading: false,
                posts: [action.payload, ...state.posts]
            }
        case 'NEW_POST_FAILED':
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        // case 'ITEM_GET_PENDING':
        //     return {
        //         ...state,
        //         loading: true
        //     }
        // case 'ITEM_GET_SUCCES':
        //     return {
        //         ...state,
        //         loading: false,
        //         items: action.payload
        //     }
        // case 'ITEM_GET_FAILED': 
        //         return {
        //             ...state,
        //             loading: false,
        //             error: action.payload
        //     }
        case 'POST_ITEM_PENDING':
            return {
                ...state,
                loading: true
            }
        case 'POST_ITEM_SUCCES' : 
            return {
                ...state,
                loading: false,
                items: action.payload
            }
        case 'POST_ITEM_FAILED':
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case 'GET_POST_PENDING':
            return {
                ...state,
                loading: true
            }
        case 'GET_POST_SUCCES' : 
            return {
                ...state,
                loading: false,
                posts: action.payload
            }
        case 'GET_POST_FAILED':
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        default:
            return state;
    }
}

//REFERENSI

/* import axios from "axios"
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom";

export default function PostDetail () {
    const [postDetail, setpostDetail] = useState([]);
    const params = useParams();

    useEffect(() => {
        const getPostDetail = async() => {
            try {
                const detailResult = await axios.get(`https://jsonplaceholder.typicode.com/posts/${params.id}`)
                setpostDetail(detailResult.data);
            } catch (error) {
                console.log("Error to load Post Detail");
            }
        }
        getPostDetail();
    }, [params.id])

    return(
        <div>
            <dt>{postDetail.title}</dt>
            <dd>{postDetail.body}</dd>
        </div>
    )
}

import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { setAction } from "./action";

function C() {
    const counter = useSelector((state) => state.counter);
    const dispatch = useDispatch();
    const [newCounter, setNewCounter] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        dispatch(setAction(newCounter));
        setNewCounter('');
    }
    return (
        <div>
            C<br/>
            {counter}<br/>
            <form onSubmit={handleSubmit}>
                <input type="number" value={newCounter} onChange={(e)=>setNewCounter(e.target.value)}/>
                <button type="submit" value="submit" >
                    Set
                </button>
            </form>
        </div>
    )
}
export default C;
*/