import axios from "axios"

const GetItems = () => {
    return(dispatch, getState) => {
        dispatch({type: 'ITEM_GET_PENDING'});
        //API call
        axios.get('https://my-json-server.typicode.com/irhamhqm/placeholder-shops/items')
            .then((response) => {
                dispatch({type: 'ITEM_GET_SUCCES', payload: response.data});
            })
            .catch((error) => {
                dispatch({type:'ITEM_GET_FAILED', payload: error });
            });
    }
}

const GetPost = () => {
    return(dispatch, getState) => {
        dispatch({type: 'GET_POST_PENDING'});
        //API Call
        axios.get('https://jsonplaceholder.typicode.com/posts')
        .then((response) => {
            dispatch({type: 'GET_POST_SUCCES', payload: response.data});
            console.log(response);
            })
            .catch((error) => {
                dispatch({type: 'GET_POST_FAILED', payload: error});
            })
    }
}

const NewPost = (data) => {
    return(dispatch, getState) => {
        dispatch({type: 'NEW_POST_PENDING'});
        //API Post
        axios.post('https://jsonplaceholder.typicode.com/posts', {
            data: JSON.stringify(data),
            headers: { "Content-Type": "application/json" },
        })
            .then((response) => {
                console.log(response.data);
                dispatch({type: 'NEW_POST_SUCCES', payload: data});
            })
            .catch((error) => {
                console.log(error);
                dispatch({type: 'NEW_POST_FAILED', payload: error});
            });
    }
}

export {
    GetItems,
    GetPost,
    NewPost
}