import './App.css';
import { useDispatch, useSelector } from 'react-redux';
import { /*GetItems, */ GetPost } from './redux/actions/actions';
import { useEffect } from 'react';
import Form from './component/form';

export default function App() {
  const dispatch = useDispatch();
  const {posts, loading, error} = useSelector((state) => state);
  
  useEffect(() => {
    dispatch(GetPost());
  }, [])
  
  return (
    <div className="app">
      <Form/>
      <div>
      <ul>
        {(loading && !error) ? <div className='loading'>Loading....</div> : 
            posts.map((post, index) => (
                <li key={index}>
                  <h4>{post.title}</h4>
                  <p>{post.body}</p>
                </li>
              ))
        }
        {error && <div>Unexpeccted Error Occured </div>}
        </ul>
      </div>
    </div>
    )
  /*    const {items, loading, error} = useSelector((state) => state);
      
      useEffect(() => {
        dispatch(GetItems());
      }, [dispatch])
    
      return (
         <>
          <div className="App">
            {(loading && !error) ? <div className='loading'> Loading....</div> :
              items.map((item) =>(
                <div key={item.uid}>{item.productName}</div>
                ))
              }
            {error && <div>Unexpeccted Error Occured </div>}
          </div>
         </>
      );
    } */
  }