import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { NewPost } from '../redux/actions/actions';

function Form() {
    const dispatch = useDispatch();
    const [newTitle, setNewTitle] = useState("");
    const [newBody, setNewBody] = useState("");  

    
    const handleSubmit = (e) => {
        e.preventDefault();

        const data = {
            title: newTitle,
            body: newBody,
        };

        dispatch(NewPost(data));
        setNewBody("");
        setNewTitle("");
      }
    
    return(
        <form onSubmit={handleSubmit}>
        <div className="new-post-container">
         <label>
             Title
             <input required type="text" value={newTitle} onChange={(e) => setNewTitle(e.target.value)} />
         </label>
         <label>
             Body
             <input required type="text" value={newBody} onChange={(e) => setNewBody(e.target.value)} />
         </label>
          <button value="submit" type="submit"> Make a Post </button>
       </div>
      </form>
    )
}

export default Form;